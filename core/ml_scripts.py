# from sklearn.compose import ColumnTransformer
# from sklearn.preprocessing import OneHotEncoder
# from sklearn.preprocessing import LabelEncoder
# from sklearn.model_selection import train_test_split
# import pandas as pd
# import numpy as np
# from joblib import load
#
# from core.models import Season
# from core.utils import data_to_dataframe, calculate_cumulative_match_day_form, save_form_data_to_database, \
#     save_season_league_table_to_database, scrape_data, save_data_to_database
#
#
# # def create_prediction_data_set(season: Season):
# #     second_season_match_day = season.starting_match_day + 1
# #     season_match_days_from_the_second = range(second_season_match_day, second_season_match_day + 34)
# #
# #     for match_day in season_match_days_from_the_second:
# #         previous_match_day_number = match_day - 1
# #         matches_from_previous_match_day = Match.objects.filter(match_day__match_day_number=previous_match_day_number,
# #                                                                match_day__season=season).all()
# #         matches_for_current_match_day = Match.objects.filter(match_day__match_day_number=match_day,
# #                                                              match_day__season=season).all()
# #
# #         for match in matches_for_current_match_day.iterator():
# #             # fixture
# #             home_team = match.home_team
# #             away_team = match.away_team
# #
# #             # get the home team's current season stats as of the previous match day accumulated
# #             # from the previous match days
# #             home_team_previous_match_day = matches_from_previous_match_day.filter(
# #                 Q(home_team=home_team) | Q(away_team=home_team)).first()
# #             # get stats
# #             home_team_wins, home_team_draws, home_team_losses, home_team_streak, home_team_position = get_form_data(
# #                 home_team, home_team_previous_match_day.match_day)
# def load_trained_model(filename='finalized_model.pkl'):
#     # load the model from disk
#     loaded_model = load(filename)
#
#     return loaded_model
#
#
# def prepare_model_data(features, labels, random_state=42):
#     # Split the data into training and testing sets
#     X_train, X_test, y_train, y_test = train_test_split(features, labels, test_size=0.2,
#                                                         random_state=random_state)
#
#     return X_train, X_test, y_train, y_test
#
#
# def convert_to_numerical_data(cols: list, df: pd.DataFrame = None, csv_file: str = None):
#     if df is None and csv_file is None:
#         return None
#
#     if df is None and csv_file is not None:
#         df = pd.read_csv(csv_file)
#
#     X = df.drop(columns=['Result'], axis=1)
#     y = df['Result']
#
#     transformer = ColumnTransformer(transformers=[
#         ('encoder', OneHotEncoder(), cols)], remainder='passthrough')
#     X_transformed = transformer.fit_transform(X)
#
#     label_encoder = LabelEncoder()
#     y_transformed = label_encoder.fit_transform(y)
#
#     # After fitting your ColumnTransformer named `preprocessor`
#     # Extract the one-hot encoder from it
#     one_hot_encoder = transformer.named_transformers_['encoder']
#
#     # Get feature names from one-hot encoder
#     feature_names = one_hot_encoder.get_feature_names_out(input_features=cols)
#
#     # numerical columns
#     num_cols = [col for col in df.columns if col not in cols + ['Result']]
#
#     # Combine feature names
#     feature_names = np.concatenate([feature_names, num_cols])
#
#     # Convert the sparse matrix to a dense NumPy array if not already
#     if isinstance(X_transformed, np.ndarray):
#         X_transformed = X_transformed
#     else:
#         X_transformed = X_transformed.toarray()
#
#     # Create a DataFrame with the new dense array and feature names
#     df_X_transformed = pd.DataFrame(X_transformed, columns=feature_names)
#
#     return df_X_transformed, y_transformed
#
#
# def train_machine_learning_model(form_df: pd.DataFrame = None, csv_file: str = 'data001.csv'):
#     if form_df is None:
#         form_df = data_to_dataframe(save_to_csv=True)
#
#     # drop Home Team HT Goals, Away Team HT Goals, Home Team Full Time Goals, Away Team Full Time Goals
#     cols_to_drop = ['Home Team Half Time Goals', 'Away Team Half Time Goals',
#                     'Home Team Full Time Goals',
#                     'Away Team Full Time Goals']
#
#     form_df.drop(columns=cols_to_drop, inplace=True)
#
#     categorical_cols = ['Home Team', 'Away Team']
#     transformed_features, transformed_labels = convert_to_numerical_data(cols=categorical_cols, df=form_df)
#
#     train_features, test_features, train_labels, test_labels = prepare_model_data(transformed_features,
#                                                                                   transformed_labels)
#
#     model = load_trained_model()
#     model.fit(train_features, train_labels)
#
#     model_accuracy = model.score(test_features, test_labels)
#
#     return model_accuracy
#
#
# # def predict_old(season_no: int):
# #     data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)
# #     form_data, league_data = calculate_cumulative_match_day_form(data)
# #     save_form_data_to_database(season_no, form_data)
# #     save_season_league_table_to_database(season_no, league_data)
# #     data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)
# #     # drop Home Team HT Goals, Away Team HT Goals, Home Team Full Time Goals, Away Team Full Time Goals
# #     # as they are not needed for model training because they give away the result of the match
# #     cols_to_drop = ['Home Team Half Time Goals', 'Away Team Half Time Goals',
# #                     'Home Team Full Time Goals',
# #                     'Away Team Full Time Goals']
# #     data.drop(columns=cols_to_drop, inplace=True)
# #
# #     # encode the categorical columns
# #     # then train model on the data
# #     # then predict the outcome of the matches
# #
# #     transformed_features, transformed_labels = convert_to_numerical_data(cols=categorical_cols, df=data)
# #     model = load_trained_model(filename='finalized_model.pkl')
# #     model_accuracy = model.score(transformed_features, transformed_labels)
# #
# #     return model_accuracy * 100
#
#
# def predict_new(season_no: int):
#     """Predict the outcome of matches in a season using the trained model."""
#     data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)
#     form_data, league_data = calculate_cumulative_match_day_form(data)
#     save_form_data_to_database(season_no, form_data)
#     save_season_league_table_to_database(season_no, league_data)
#     data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)
#     cols_to_drop = ['Home Team Half Time Goals', 'Away Team Half Time Goals',
#                     'Home Team Full Time Goals',
#                     'Away Team Full Time Goals']
#     categorical_cols = ['Home Team', 'Away Team']
#     data.drop(columns=cols_to_drop, inplace=True)
#     transformed_features, transformed_labels = convert_to_numerical_data(cols=categorical_cols, df=data)
#     model = load_trained_model(filename='finalized_model.pkl')
#     model_accuracy = model.score(transformed_features, transformed_labels)
#
#     return model_accuracy * 100
#
#
# def grab_and_predict_season(season_number):
#     season_number = season_number
#     season = Season.objects.get(season_number=season_number)
#     ex_data = scrape_data(season)
#     save_data_to_database(season.id, ex_data)
#
#     predict_new(season)
