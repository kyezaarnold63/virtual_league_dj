from django.core.exceptions import ObjectDoesNotExist
from django.db.utils import IntegrityError
from joblib import dump
import re

from selenium.webdriver.chrome import webdriver

import pandas as pd
from selenium import webdriver
from selenium.webdriver.chrome.service import Service as ChromeService
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from sklearn.ensemble import RandomForestClassifier
from webdriver_manager.chrome import ChromeDriverManager
from selenium.webdriver.support import expected_conditions as EC

from core.models import Season, MatchDay, Match, Team, MatchDayFormTable, SeasonPositionTable


def match_result(score):
    # Extract the home and away team scores
    home_score, away_score = score.split(' - ')

    if home_score == '' or away_score == '':
        return 'not_played'

    # Return the result of the match
    if home_score > away_score:
        return 'home_win'
    elif home_score < away_score:
        return 'away_win'
    else:
        return 'draw'


def extract_match_day_data(data_string):
    # Split the data string into rows
    rows = [r for r in data_string.split('\n') if r != '']

    # Create an empty list to store the data for each match
    match_data = []

    # Loop through the rows
    for md_row in rows:
        # if a match has not been played, the row data looks like this, row = 'ARS - SOU -'
        # else, the row data looks like this, row = 'ARS - SOU (0 - 1) 1 - 2'
        # Extract the home and away teams only if match has not been played
        # and replace the missing value of half_time_score with (' - ') and full_time_score with (' - ')
        # Extract the home team, away team, first half score, and full time score for each match
        # given row = 'AST - BHA (0 - 1) 1 - 2'

        if md_row[-1] == '-':
            md_row = md_row.strip('-').strip()
            home_team, away_team = re.split(' - ', md_row)
            first_half_score = ' - '
            full_time_score = ' - '
        else:
            home_team, away_team, first_half_score, full_time_score = \
                re.findall(r'(\w{3}) - (\w{3}) \((\d{1} - \d{1})\) (\d{1} - \d{1})', md_row)[0]

        # Append the data for each match to the list
        match_data.append([home_team, away_team, first_half_score, full_time_score, match_result(full_time_score)])

    # Create a Pandas dataframe from the list of match data
    return pd.DataFrame(match_data, columns=['home_team', 'away_team', 'first_half_score', 'full_time_score', 'result'])


def scrape_data(season: Season):
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument("--headless")

    driver = webdriver.Chrome(service=ChromeService(ChromeDriverManager().install()),
                              options=chrome_options)

    tables_data = []
    season, first_game_wk = season.season_number, season.starting_match_day

    for md in range(first_game_wk, first_game_wk + 34):
        url = f'https://www.betpawa.ug/virtual-sports/matchday/{season}/{md}'
        driver.get(url)

        try:
            # find the table element
            tables = WebDriverWait(driver, 10).until(
                EC.presence_of_all_elements_located((By.CSS_SELECTOR, "[class='events-container']"))
            )
            tables = tables[:10]
            game_wk_results = ''
            for row in tables:
                game_wk_results += row.text.replace('\n', ' ') + '\n'

            tables_data.append(game_wk_results)

        except Exception as e:
            print(e)
            driver.quit()

    driver.quit()

    extracted_data = {}

    for index, md_data in enumerate(tables_data):
        data = extract_match_day_data(md_data)
        match_day = index + 1
        match_day_number = first_game_wk + index

        extracted_data[match_day] = (match_day_number, data)

    return extracted_data


def save_data_to_database(season_id: int, data: dict, update: bool = False):
    for match_day, (match_day_number, match_data) in data.items():
        try:
            game_day = MatchDay.objects.create(match_day_number=match_day_number,
                                               match_day=match_day, season_id=season_id)
        except IntegrityError:
            if update:
                game_day = MatchDay.objects.get(match_day_number=match_day_number, season_id=season_id)
            else:
                continue

        for index, row in match_data.iterrows():
            home_team = Team.objects.get(name=row['home_team'])
            away_team = Team.objects.get(name=row['away_team'])
            played = True
            if not any(x == '' for x in row['first_half_score'].split(' - ')):
                home_team_first_half_goals, away_team_first_half_goals = (int(x.strip()) for x in
                                                                          row['first_half_score'].split(' - '))
            else:
                home_team_first_half_goals, away_team_first_half_goals = None, None
                played = False

            if not any(x == '' for x in row['full_time_score'].split(' - ')):
                home_team_full_time_goals, away_team_full_time_goals = (int(x.strip()) for x in
                                                                        row['full_time_score'].split(' - '))
            else:
                home_team_full_time_goals, away_team_full_time_goals = None, None
                played = False

            if played:
                result = row['result'][0].capitalize()
            else:
                result = None

            if update:
                match = Match.objects.get(home_team=home_team, away_team=away_team, match_day=game_day)
                match.home_team_half_time_goals = home_team_first_half_goals
                match.away_team_half_time_goals = away_team_first_half_goals
                match.home_team_full_time_goals = home_team_full_time_goals
                match.away_team_full_time_goals = away_team_full_time_goals
                match.result = result
                match.save()
            else:
                # save the match data to the database
                Match.objects.create(home_team=home_team,
                                     away_team=away_team,
                                     home_team_half_time_goals=home_team_first_half_goals,
                                     away_team_half_time_goals=away_team_first_half_goals,
                                     home_team_full_time_goals=home_team_full_time_goals,
                                     away_team_full_time_goals=away_team_full_time_goals,
                                     match_day=game_day,
                                     result=result)


def get_form_data(team, match_day):
    try:
        form_entry = MatchDayFormTable.objects.get(team=team, match_day=match_day)
        return (
            form_entry.wins,
            form_entry.draws,
            form_entry.losses,
            form_entry.streak,
            form_entry.team_position)
    except MatchDayFormTable.DoesNotExist:
        return 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'


def get_position_data(team, season):
    try:
        position_entry = SeasonPositionTable.objects.get(team=team, season=season)
        return (
            position_entry.goals_for,
            position_entry.goals_against,
            position_entry.goal_difference,
            position_entry.points)
    except SeasonPositionTable.DoesNotExist:
        return 'N/A', 'N/A', 'N/A', 'N/A'


def data_to_dataframe(season_number: int = 'all', match_day: int = None, all_data: bool = False,
                      save_to_csv: bool = False):
    if all_data is False and match_day is not None:
        # matches data for a specific match day
        qs = Match.objects.filter(match_day__match_day=match_day, match_day__season__season_number=season_number,
                                  result__isnull=False).all()
    elif all_data is True and match_day is None:
        # matches data for all match days in a season
        qs = Match.objects.filter(match_day__season__season_number=season_number, result__isnull=False).all()
    else:
        # matches data for all match days in all seasons
        qs = Match.objects.filter(result__isnull=False).all()

    data = []
    for match in qs.iterator():
        season = match.match_day.season.season_number
        match_day_number = match.match_day.match_day_number
        match_day = match.match_day.match_day
        home_team = match.home_team.name
        away_team = match.away_team.name
        home_team_half_time_goals = match.home_team_half_time_goals
        away_team_half_time_goals = match.away_team_half_time_goals
        home_team_full_time_goals = match.home_team_full_time_goals
        away_team_full_time_goals = match.away_team_full_time_goals
        result = match.result

        home_team_wins, home_team_draws, home_team_losses, home_team_streak, home_team_position = get_form_data(
            match.home_team, match.match_day)
        away_team_wins, away_team_draws, away_team_losses, away_team_streak, away_team_position = get_form_data(
            match.away_team, match.match_day)

        home_team_goals_for, home_team_goals_against, home_team_goal_difference, home_team_points = get_position_data(
            match.home_team, match.match_day.season)
        away_team_goals_for, away_team_goals_against, away_team_goal_difference, away_team_points = get_position_data(
            match.away_team, match.match_day.season)

        data.append(
            [season, match_day_number, match_day, home_team, away_team, home_team_half_time_goals,
             away_team_half_time_goals, home_team_full_time_goals, away_team_full_time_goals, result,
             home_team_wins, home_team_draws, home_team_losses, home_team_streak, home_team_position,
             away_team_wins, away_team_draws, away_team_losses, away_team_streak, away_team_position,
             home_team_goals_for, home_team_goals_against, home_team_goal_difference, home_team_points,
             away_team_goals_for, away_team_goals_against, away_team_goal_difference, away_team_points]
        )

    cols = ['Season', 'Match Day ID', 'Match Day', 'Home Team', 'Away Team', 'Home Team Half Time Goals',
            'Away Team Half Time Goals', 'Home Team Full Time Goals', 'Away Team Full Time Goals', 'Result',
            'Home Team Wins', 'Home Team Draws', 'Home Team Losses', 'Home Team Streak', 'Home Team Position',
            'Away Team Wins', 'Away Team Draws', 'Away Team Losses', 'Away Team Streak', 'Away Team Position',
            'Home Team Goals For', 'Home Team Goals Against', 'Home Team Goal Difference', 'Home Team Points',
            'Away Team Goals For', 'Away Team Goals Against', 'Away Team Goal Difference', 'Away Team Points']

    df = pd.DataFrame(data, columns=cols)

    if save_to_csv:
        df.to_csv(f'{season_number}.csv', index=False)

    return df


def generate_ml_data(season_number: int):
    """
    Generate the data for training in the machine learning model
    :param season_number: season number for which to generate the data
    :return:
    """
    season_data = pd.DataFrame(columns=[
        'Home Team', 'Away Team', 'Home Team Wins', 'Home Team Draws', 'Home Team Losses', 'Home Team Streak',
        'Home Team Current Position', 'Away Team Wins', 'Away Team Draws', 'Away Team Losses', 'Away Team Streak',
        'Away Team Current Position', 'Result']

    )
    starting_match_day_number, ending_match_day_number = (list(Season.objects.filter(season_number__exact=season_number)
                                                               .values_list('starting_match_day', 'end_match_day'))[0])

    for match_day, match_day_number in enumerate(range(starting_match_day_number, ending_match_day_number + 1)):
        match_day += 1
        match_day_data = pd.DataFrame(columns=[
            'Home Team', 'Away Team', 'Home Team Wins', 'Home Team Draws', 'Home Team Losses', 'Home Team Streak',
            'Home Team Current Position', 'Away Team Wins', 'Away Team Draws', 'Away Team Losses', 'Away Team Streak',
            'Away Team Current Position', 'Result'])

        current_match_day_fixtures = list(Match.objects.filter(match_day__match_day_number=match_day_number,
                                                               result__isnull=False)
                                          .values('home_team__name', 'away_team__name', 'result'))

        if not current_match_day_fixtures:
            continue

        previous_match_day_data = MatchDayFormTable.objects.filter(
            match_day__match_day_number=(match_day_number - 1)).all()

        if not previous_match_day_data:
            continue

        for fixture in current_match_day_fixtures:
            try:
                if match_day == 1:
                    # add the fixture to the match_day_data data frame
                    # since it's the first match day, there's no previous match day data
                    # so replace the rest of the columns with 0s

                    new_row = pd.DataFrame({
                        'Home Team': [fixture['home_team__name']],
                        'Away Team': [fixture['away_team__name']],
                        'Home Team Wins': [0],
                        'Home Team Draws': [0],
                        'Home Team Losses': [0],
                        'Home Team Streak': [0],
                        'Home Team Current Position': [0],
                        'Away Team Wins': [0],
                        'Away Team Draws': [0],
                        'Away Team Losses': [0],
                        'Away Team Streak': [0],
                        'Away Team Current Position': [0],
                        'Result': [fixture['result']]
                    })
                    match_day_data = pd.concat([match_day_data, new_row], ignore_index=True)
                else:
                    # get the home team's previous match day's data from previous_match_day_data
                    home_team = fixture['home_team__name']
                    home_team_previous_match_day_data = previous_match_day_data.filter(team__name=home_team).first()
                    home_team_wins = home_team_previous_match_day_data.wins
                    home_team_draws = home_team_previous_match_day_data.draws
                    home_team_losses = home_team_previous_match_day_data.losses
                    home_team_streak = home_team_previous_match_day_data.streak
                    home_team_current_position = home_team_previous_match_day_data.team_position

                    # get the away team's previous match day's data from previous_match_day_data
                    away_team = fixture['away_team__name']
                    away_team_previous_match_day_data = previous_match_day_data.filter(team__name=away_team).first()
                    away_team_wins = away_team_previous_match_day_data.wins
                    away_team_draws = away_team_previous_match_day_data.draws
                    away_team_losses = away_team_previous_match_day_data.losses
                    away_team_streak = away_team_previous_match_day_data.streak
                    away_team_current_position = away_team_previous_match_day_data.team_position

                    # add the fixture to the match_day_data data frame
                    new_row = pd.DataFrame({
                        'Home Team': [home_team],
                        'Away Team': [away_team],
                        'Home Team Wins': [home_team_wins],
                        'Home Team Draws': [home_team_draws],
                        'Home Team Losses': [home_team_losses],
                        'Home Team Streak': [home_team_streak],
                        'Home Team Current Position': [home_team_current_position],
                        'Away Team Wins': [away_team_wins],
                        'Away Team Draws': [away_team_draws],
                        'Away Team Losses': [away_team_losses],
                        'Away Team Streak': [away_team_streak],
                        'Away Team Current Position': [away_team_current_position],
                        'Result': [fixture['result']]
                    })
                    match_day_data = pd.concat([match_day_data, new_row], ignore_index=True)
            except Exception as e:
                print(e)
                print(f'season_number: {season_number}, match_day: {match_day}, match_day_number: {match_day_number}, '
                      f'previous_match_day_number: {match_day_number - 1}')
                raise

        # add the match_day_data to the season_data data frame
        season_data = pd.concat([season_data, match_day_data], ignore_index=True)

    return season_data


def data_for_all_seasons(filename: str = None):
    """
    Generate the training data for all seasons without IDs and some other columns
    :param filename: name to save the csv file
    :return: data frame of all the data
    """
    all_data = pd.DataFrame(columns=[
        'Home Team', 'Away Team', 'Home Team Wins', 'Home Team Draws', 'Home Team Losses', 'Home Team Streak',
        'Home Team Current Position', 'Away Team Wins', 'Away Team Draws', 'Away Team Losses', 'Away Team Streak',
        'Away Team Current Position', 'Result'
    ])

    for season in Season.objects.all():
        season_data = generate_ml_data(season.season_number)

        all_data = pd.concat([all_data, season_data], ignore_index=True)

    csv_file_name = filename if filename is not None else 'all_data.csv'
    all_data.to_csv(csv_file_name, index=False)

    return all_data


def create_and_train_model(training_features_set, training_labels_set, random_state=42,
                           save_file_name='finalized_model.pkl'):
    # Create the model
    model = RandomForestClassifier(n_estimators=100, random_state=random_state)
    model.fit(training_features_set, training_labels_set)

    # Save the model
    dump(model, save_file_name)

    return model


def calculate_cumulative_match_day_form(df: pd.DataFrame):
    # Initialize an empty DataFrame to store form information
    form_df = pd.DataFrame(columns=['Match Day', 'Team', 'Wins', 'Draws', 'Losses', 'Streak'])
    league_table_df = pd.DataFrame(
        columns=['Match Day ID', 'Match Day', 'Team', 'Wins', 'Draws', 'Losses', 'Goals_For', 'Goals_Against',
                 'Goal_Difference',
                 'Points'])

    # Loop through the data to calculate form cumulatively
    gw_counter = 0
    for game_week in df['Match Day'].unique():
        week_form_dict = {}
        week_league_dict = {}

        # Create a smaller DataFrame containing only the games up to this game week
        current_weeks_df = df[df['Match Day'].le(game_week)]
        match_day_id = current_weeks_df['Match Day ID'].unique()[gw_counter]

        for _, row in current_weeks_df.iterrows():
            home_team = row['Home Team']
            away_team = row['Away Team']
            result = row['Result']
            home_goals = row['Home Team Full Time Goals']
            away_goals = row['Away Team Full Time Goals']

            if home_team not in week_form_dict:
                week_form_dict[home_team] = {'Wins': 0, 'Draws': 0, 'Losses': 0, 'Streak': 0}

            if away_team not in week_form_dict:
                week_form_dict[away_team] = {'Wins': 0, 'Draws': 0, 'Losses': 0, 'Streak': 0}

            if home_team not in week_league_dict:
                week_league_dict[home_team] = {'Wins': 0, 'Draws': 0, 'Losses': 0, 'Goals_For': 0,
                                               'Goals_Against': 0, 'Goal_Difference': 0, 'Points': 0}

            if away_team not in week_league_dict:
                week_league_dict[away_team] = {'Wins': 0, 'Draws': 0, 'Losses': 0, 'Goals_For': 0,
                                               'Goals_Against': 0, 'Goal_Difference': 0, 'Points': 0}

            if result == 'H':
                week_form_dict[home_team]['Wins'] += 1
                week_form_dict[home_team]['Streak'] = week_form_dict[home_team]['Streak'] + 1 \
                    if week_form_dict[home_team]['Streak'] >= 0 else 1

                week_form_dict[away_team]['Losses'] += 1
                week_form_dict[away_team]['Streak'] = week_form_dict[away_team]['Streak'] - 1 \
                    if week_form_dict[away_team]['Streak'] <= 0 else -1

                week_league_dict[home_team]['Wins'] += 1
                week_league_dict[home_team]['Goals_For'] += home_goals
                week_league_dict[home_team]['Goals_Against'] += away_goals
                week_league_dict[home_team]['Goal_Difference'] += home_goals - away_goals
                week_league_dict[home_team]['Points'] += 3

                week_league_dict[away_team]['Losses'] += 1
                week_league_dict[away_team]['Goals_For'] += away_goals
                week_league_dict[away_team]['Goals_Against'] += home_goals
                week_league_dict[away_team]['Goal_Difference'] += away_goals - home_goals

            elif result == 'A':
                week_form_dict[home_team]['Losses'] += 1
                week_form_dict[home_team]['Streak'] = week_form_dict[home_team]['Streak'] - 1 \
                    if week_form_dict[home_team]['Streak'] <= 0 else -1

                week_form_dict[away_team]['Wins'] += 1
                week_form_dict[away_team]['Streak'] = week_form_dict[away_team]['Streak'] + 1 \
                    if week_form_dict[away_team]['Streak'] >= 0 else 1

                week_league_dict[away_team]['Wins'] += 1
                week_league_dict[away_team]['Goals_For'] += away_goals
                week_league_dict[away_team]['Goals_Against'] += home_goals
                week_league_dict[away_team]['Goal_Difference'] += away_goals - home_goals
                week_league_dict[away_team]['Points'] += 3

                week_league_dict[home_team]['Losses'] += 1
                week_league_dict[home_team]['Goals_For'] += home_goals
                week_league_dict[home_team]['Goals_Against'] += away_goals
                week_league_dict[home_team]['Goal_Difference'] += home_goals - away_goals
            else:
                week_form_dict[home_team]['Draws'] += 1
                week_form_dict[home_team]['Streak'] = 0

                week_form_dict[away_team]['Draws'] += 1
                week_form_dict[away_team]['Streak'] = 0

                week_league_dict[away_team]['Draws'] += 1
                week_league_dict[away_team]['Goals_For'] += away_goals
                week_league_dict[away_team]['Goals_Against'] += home_goals
                week_league_dict[away_team]['Goal_Difference'] += away_goals - home_goals
                week_league_dict[away_team]['Points'] += 1

                week_league_dict[home_team]['Draws'] += 1
                week_league_dict[home_team]['Goals_For'] += home_goals
                week_league_dict[home_team]['Goals_Against'] += away_goals
                week_league_dict[home_team]['Goal_Difference'] += home_goals - away_goals
                week_league_dict[home_team]['Points'] += 1

        for team, form in week_form_dict.items():
            new_row = pd.DataFrame({'Match Day': [game_week], 'Team': [team], **form})
            form_df = pd.concat([form_df, new_row], ignore_index=True)

        for team, stats in week_league_dict.items():
            new_row = pd.DataFrame({'Match Day ID': [match_day_id], 'Match Day': [game_week], 'Team': [team], **stats})
            league_table_df = pd.concat([league_table_df, new_row], ignore_index=True)

        gw_counter += 1

    # Merge form_df with the original DataFrame for home and away teams
    df = pd.merge(df, form_df, left_on=['Match Day', 'Home Team'], right_on=['Match Day', 'Team'],
                  suffixes=('', '_Home'))
    df = pd.merge(df, form_df, left_on=['Match Day', 'Away Team'], right_on=['Match Day', 'Team'],
                  suffixes=('', '_Away'))

    # Drop unnecessary columns
    df.drop(columns=['Team', 'Team_Away'], inplace=True)

    # Rename columns
    df.rename(columns={
        'Wins': 'Home_Team_Wins',
        'Draws': 'Home_Team_Draws',
        'Losses': 'Home_Team_Losses',
        'Streak': 'Home_Team_Streak',
        'Wins_Away': 'Away_Team_Wins',
        'Draws_Away': 'Away_Team_Draws',
        'Losses_Away': 'Away_Team_Losses',
        'Streak_Away': 'Away_Team_Streak'
    }, inplace=True)

    # Calculate the league positions for each match day
    for game_week in df['Match Day ID'].unique():
        # a mask for the match day which a combination of the match day number and the match day

        week_table = league_table_df[league_table_df['Match Day ID'] == game_week]
        week_table = week_table.sort_values(by=['Points', 'Goal_Difference', 'Goals_For'],
                                            ascending=[False, False, False])
        week_table = week_table.reset_index(drop=True)
        week_table['Position'] = range(1, len(week_table) + 1)

        # Update only the 'Position' column in league_table_df for the current game_week
        for idx, row in week_table.iterrows():
            match_day_team_mask = ((league_table_df['Match Day ID'] == game_week) &
                                   (league_table_df['Team'] == row['Team']))
            league_table_df.loc[match_day_team_mask, 'Position'] = row['Position']

    # Merge league_table_df with the original DataFrame for home and away teams
    df = pd.merge(df, league_table_df[['Match Day', 'Team', 'Position']], left_on=['Match Day', 'Home Team'],
                  right_on=['Match Day', 'Team'], suffixes=('', '_Home'))
    df.rename(columns={'Position': 'Home_Team_Position'}, inplace=True)
    df.drop(columns=['Team'], inplace=True)

    df = pd.merge(df, league_table_df[['Match Day', 'Team', 'Position']], left_on=['Match Day', 'Away Team'],
                  right_on=['Match Day', 'Team'], suffixes=('', '_Away'))
    df.rename(columns={'Position': 'Away_Team_Position'}, inplace=True)
    df.drop(columns=['Team'], inplace=True)

    return df, league_table_df


def save_season_league_table_to_database(season_number: int, df: pd.DataFrame):
    for index, row in df.iterrows():
        match_day_number = row['Match Day ID']
        match_day = row['Match Day']
        team = row['Team']
        wins = row['Wins']
        draws = row['Draws']
        losses = row['Losses']
        goals_for = row['Goals_For']
        goals_against = row['Goals_Against']
        goal_difference = row['Goal_Difference']
        points = row['Points']

        # calculate the team's league position
        # mask for match day number and match day
        mask = (df['Match Day ID'] == match_day_number) & (df['Match Day'] == match_day)
        position = df[mask].sort_values(by=['Points', 'Goal_Difference', 'Goals_For'], ascending=False).index.get_loc(
            index) + 1

        # Fetch the match day and team
        try:
            match_day = MatchDay.objects.get(match_day_number=match_day_number,
                                             match_day=match_day,
                                             season__season_number=season_number)
        except ObjectDoesNotExist:
            print(f"Match day does not exist for row {index}.")
            continue

        try:
            team = Team.objects.get(name=team)
        except ObjectDoesNotExist:
            print(f"Team does not exist for row {index}.")
            continue

        # Save the form data to the database
        obj, _ = SeasonPositionTable.objects.get_or_create(season=match_day.season,
                                                           team=team)
        obj.position = position
        obj.wins = wins
        obj.draws = draws
        obj.losses = losses
        obj.goals_for = goals_for
        obj.goals_against = goals_against
        obj.goal_difference = goal_difference
        obj.points = points
        obj.save()


def save_form_data_to_database(season_number: int, df: pd.DataFrame):
    for index, row in df.iterrows():
        match_day_number = row['Match Day ID']
        match_day = row['Match Day']
        home_team = row['Home Team']
        away_team = row['Away Team']
        home_team_wins = row['Home_Team_Wins']
        home_team_draws = row['Home_Team_Draws']
        home_team_losses = row['Home_Team_Losses']
        home_team_streak = row['Home_Team_Streak']
        away_team_wins = row['Away_Team_Wins']
        away_team_draws = row['Away_Team_Draws']
        away_team_losses = row['Away_Team_Losses']
        away_team_streak = row['Away_Team_Streak']
        home_team_position = row['Home_Team_Position']
        away_team_position = row['Away_Team_Position']

        # Fetch the match day and team
        try:
            match_day = MatchDay.objects.get(match_day_number=match_day_number,
                                             match_day=match_day,
                                             season__season_number=season_number)
        except ObjectDoesNotExist:
            print(f"Match day does not exist for row {index}.")
            continue

        try:
            home_team = Team.objects.get(name=home_team)
            away_team = Team.objects.get(name=away_team)
        except ObjectDoesNotExist:
            print(f"Team does not exist for row {index}.")
            continue

        # Save the form data to the database
        try:
            MatchDayFormTable.objects.create(match_day=match_day, team=home_team,
                                             wins=home_team_wins, draws=home_team_draws, losses=home_team_losses,
                                             streak=home_team_streak, team_position=home_team_position)
        except IntegrityError:
            pass

        try:
            MatchDayFormTable.objects.create(match_day=match_day, team=away_team,
                                             wins=away_team_wins, draws=away_team_draws, losses=away_team_losses,
                                             streak=away_team_streak, team_position=away_team_position)
        except IntegrityError:
            pass


def run_transformation_flow(season_number: int, save_form_data: bool = False):
    data = data_to_dataframe(season_number=season_number, all_data=True, save_to_csv=True)
    form_df, table_df = calculate_cumulative_match_day_form(data)

    if save_form_data:
        save_form_data_to_database(season_number, form_df)

    save_season_league_table_to_database(season_number, table_df)

    return form_df


def create_new_season(seasons_start_from, season_md_end, seasons_end):
    season = seasons_start_from + 1

    for s in range(season, seasons_end + 1):
        md1 = season_md_end + 1
        md2 = md1 + 33
        Season.objects.create(name=f'#{s}', season_number=s, starting_match_day=md1, end_match_day=md2)
        season_md_end = md2

    print(f"Seasons created successfully.")