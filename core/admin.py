from django.contrib import admin
from .models import (Season, Team, MatchDay, Match,
                     MatchDayFormTable, SeasonPositionTable)
from .views import export_to_csv


class SeasonAdmin(admin.ModelAdmin):
    list_display = ('season_number', 'name', 'starting_match_day', 'end_match_day')


class TeamAdmin(admin.ModelAdmin):
    list_display = ('name',)


class MatchDayAdmin(admin.ModelAdmin):
    list_display = ('match_day_number', 'match_day', 'season')
    list_filter = ('season__name',)


class MatchDayFormTableAdmin(admin.ModelAdmin):
    list_display = ('get_match_day_number', 'match_day', 'team', 'wins', 'draws', 'losses')
    list_filter = ('match_day__match_day_number', 'match_day__match_day',)

    def get_match_day_number(self, obj):
        return obj.match_day.match_day_number

    get_match_day_number.admin_order_field = 'match_day__match_day_number'  # Allow sorting by season
    get_match_day_number.short_description = 'Match Day Number'


class MatchAdmin(admin.ModelAdmin):
    list_display = (
        'get_season', 'get_match_day_number', 'get_match_day', 'home_team', 'away_team', 'home_team_half_time_goals',
        'away_team_half_time_goals', 'home_team_full_time_goals', 'away_team_full_time_goals',
        'result','get_home_team_wins', 'get_home_team_draws', 'get_home_team_losses',
        'get_away_team_wins', 'get_away_team_draws', 'get_away_team_losses', 'get_home_team_streak',
        'get_away_team_streak', 'get_home_team_position', 'get_away_team_position'
    )
    list_filter = ('match_day__match_day', 'home_team', 'away_team')
    actions = [export_to_csv]
    search_fields = ('match_day__season__name',)

    def get_season(self, obj):
        return obj.match_day.season.name

    get_season.admin_order_field = 'match_day__season'  # Allow sorting by season
    get_season.short_description = 'Season'

    def get_match_day_number(self, obj):
        return obj.match_day.match_day_number

    get_match_day_number.admin_order_field = 'match_day__match_day_number'  # Allow sorting by match_day_number
    get_match_day_number.short_description = 'Match Day Number'

    def get_form_data(self, team, match_day):
        try:
            form_entry = MatchDayFormTable.objects.get(team=team, match_day=match_day)
            return form_entry.wins, form_entry.draws, form_entry.losses, form_entry.streak, form_entry.team_position
        except MatchDayFormTable.DoesNotExist:
            return 'N/A', 'N/A', 'N/A', 'N/A', 'N/A'

    def get_match_day(self, obj):
        return obj.match_day.match_day

    get_match_day.admin_order_field = 'match_day__match_day'  # Allow sorting by match_day
    get_match_day.short_description = 'Match Day'

    def get_home_team_wins(self, obj):
        return self.get_form_data(obj.home_team, obj.match_day)[0]

    get_home_team_wins.short_description = 'Home Team Wins'

    def get_home_team_draws(self, obj):
        return self.get_form_data(obj.home_team, obj.match_day)[1]

    get_home_team_draws.short_description = 'Home Team Draws'

    def get_home_team_losses(self, obj):
        return self.get_form_data(obj.home_team, obj.match_day)[2]

    get_home_team_losses.short_description = 'Home Team Losses'

    def get_away_team_wins(self, obj):
        return self.get_form_data(obj.away_team, obj.match_day)[0]

    get_away_team_wins.short_description = 'Away Team Wins'

    def get_away_team_draws(self, obj):
        return self.get_form_data(obj.away_team, obj.match_day)[1]

    get_away_team_draws.short_description = 'Away Team Draws'

    def get_away_team_losses(self, obj):
        return self.get_form_data(obj.away_team, obj.match_day)[2]

    get_away_team_losses.short_description = 'Away Team Losses'

    def get_home_team_streak(self, obj):
        return self.get_form_data(obj.home_team, obj.match_day)[3]

    get_home_team_streak.short_description = 'Home Team Streak'

    def get_away_team_streak(self, obj):
        return self.get_form_data(obj.away_team, obj.match_day)[3]

    get_away_team_streak.short_description = 'Away Team Streak'

    def get_home_team_position(self, obj):
        return self.get_form_data(obj.home_team, obj.match_day)[4]

    get_home_team_position.short_description = 'Home Team Position'

    def get_away_team_position(self, obj):
        return self.get_form_data(obj.away_team, obj.match_day)[4]

    get_away_team_position.short_description = 'Away Team Position'


class SeasonPositionTableAdmin(admin.ModelAdmin):
    list_display = ('season', 'position', 'team', 'wins', 'draws', 'losses', 'goals_for', 'goals_against',
                    'goal_difference', 'points')
    list_filter = ('team__name',)
    search_fields = ('team__name', 'season__name')


# Register your models and their corresponding admin classes here.
admin.site.register(Season, SeasonAdmin)
admin.site.register(Team, TeamAdmin)
admin.site.register(MatchDay, MatchDayAdmin)
admin.site.register(Match, MatchAdmin)
admin.site.register(MatchDayFormTable, MatchDayFormTableAdmin)
admin.site.register(SeasonPositionTable, SeasonPositionTableAdmin)
