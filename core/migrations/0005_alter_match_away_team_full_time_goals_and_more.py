# Generated by Django 4.2.5 on 2023-09-10 14:58

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0004_alter_match_options'),
    ]

    operations = [
        migrations.AlterField(
            model_name='match',
            name='away_team_full_time_goals',
            field=models.PositiveIntegerField(verbose_name='Away Team FT Goals'),
        ),
        migrations.AlterField(
            model_name='match',
            name='away_team_half_time_goals',
            field=models.PositiveIntegerField(verbose_name='Away Team HT Goals'),
        ),
        migrations.AlterField(
            model_name='match',
            name='home_team_full_time_goals',
            field=models.PositiveIntegerField(verbose_name='Home Team FT Goals'),
        ),
        migrations.AlterField(
            model_name='match',
            name='home_team_half_time_goals',
            field=models.PositiveIntegerField(verbose_name='Home Team HT Goals'),
        ),
    ]
