# Generated by Django 4.2.5 on 2023-09-11 02:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0007_matchdayformtable'),
    ]

    operations = [
        migrations.AlterUniqueTogether(
            name='matchdayformtable',
            unique_together={('match_day', 'team')},
        ),
    ]
