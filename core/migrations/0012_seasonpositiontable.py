# Generated by Django 4.2.5 on 2023-09-11 19:21

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0011_alter_matchdayformtable_streak'),
    ]

    operations = [
        migrations.CreateModel(
            name='SeasonPositionTable',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('position', models.PositiveIntegerField(default=0)),
                ('wins', models.PositiveIntegerField(default=0)),
                ('draws', models.PositiveIntegerField(default=0)),
                ('losses', models.PositiveIntegerField(default=0)),
                ('goals_for', models.PositiveIntegerField(default=0)),
                ('goals_against', models.PositiveIntegerField(default=0)),
                ('goal_difference', models.IntegerField(default=0)),
                ('points', models.PositiveIntegerField(default=0)),
                ('season', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.season')),
                ('team', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='core.team')),
            ],
            options={
                'verbose_name': 'League Table',
                'verbose_name_plural': 'League Table',
                'ordering': ('season__season_number', 'position'),
                'unique_together': {('season', 'team')},
            },
        ),
    ]
