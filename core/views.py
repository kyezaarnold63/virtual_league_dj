import csv
from django.http import HttpResponse


def export_to_csv(modeladmin, request, queryset):
    opts = modeladmin.model._meta
    field_names = [field.name for field in opts.fields]
    custom_field_names = [  # Add your custom fields here
        'get_season', 'get_match_day_number', 'get_match_day', 'home_team', 'away_team', 'home_team_half_time_goals',
        'away_team_half_time_goals', 'home_team_full_time_goals', 'away_team_full_time_goals',
        'result','get_home_team_wins', 'get_home_team_draws', 'get_home_team_losses',
        'get_away_team_wins', 'get_away_team_draws', 'get_away_team_losses', 'get_home_team_streak',
        'get_away_team_streak', 'get_home_team_position', 'get_away_team_position'
    ]

    response = HttpResponse(content_type='text/csv')
    response['Content-Disposition'] = f'attachment; filename={opts}.csv'
    writer = csv.writer(response)

    # Write headers
    writer.writerow(list(field_names) + custom_field_names)

    # Write data rows
    for obj in queryset:
        data_row = []
        for field in field_names:
            data_row.append(getattr(obj, field))

        for custom_field in custom_field_names:
            method = getattr(modeladmin, custom_field, None)
            if callable(method):
                data_row.append(method(obj))

        writer.writerow(data_row)

    return response
