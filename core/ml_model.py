import pandas as pd
from django.db.models import Max
from sklearn.preprocessing import LabelEncoder
from joblib import dump, load
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score, classification_report
import joblib

from core.models import Season, Match, MatchDayFormTable, SeasonPositionTable
from core.utils import data_to_dataframe, calculate_cumulative_match_day_form, save_form_data_to_database, \
    save_season_league_table_to_database, scrape_data, save_data_to_database

FEATURES = ['Home Team', 'Away Team', 'Home Team Wins', 'Home Team Draws',
            'Home Team Losses', 'Home Team Streak', 'Home Team Position',
            'Away Team Wins', 'Away Team Draws', 'Away Team Losses',
            'Away Team Streak', 'Away Team Position', 'Home Team Goal Difference',
            'Home Team Goals For', 'Home Team Goals Against', 'Home Team Points',
            'Away Team Goal Difference', 'Away Team Goals For', 'Away Team Goals Against', 'Away Team Points']


def get_all_team_names():
    teams = set()
    matches = Match.objects.all()
    for match in matches:
        teams.add(match.home_team.name)
        teams.add(match.away_team.name)
    return list(teams)


def preprocess_training_data(season_no=None):
    # Step 1: Load and transform data
    if season_no is None:
        data = data_to_dataframe()
    else:
        data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)
        form_data, league_data = calculate_cumulative_match_day_form(data)
        save_form_data_to_database(season_no, form_data)
        save_season_league_table_to_database(season_no, league_data)
        data = data_to_dataframe(season_no, all_data=True, save_to_csv=True)

    # Step 2: Drop columns that give away match result
    cols_to_drop = ['Home Team Half Time Goals', 'Away Team Half Time Goals',
                    'Home Team Full Time Goals', 'Away Team Full Time Goals']
    data.drop(columns=cols_to_drop, inplace=True)

    # Step 3: Encode categorical variables
    all_teams = get_all_team_names()
    label_encoder = LabelEncoder()
    label_encoder.fit(all_teams)
    data['Home Team'] = label_encoder.transform(data['Home Team'])
    data['Away Team'] = label_encoder.transform(data['Away Team'])
    data['Result'] = label_encoder.fit_transform(data['Result'])

    # Step 4: Prepare features and labels
    X = data[FEATURES]
    y = data['Result']

    return X, y, label_encoder


def create_prediction_dataframe(season_number, label_encoder_filename='label_encoder.pkl'):
    # Load the label encoder
    label_encoder = joblib.load(label_encoder_filename)

    # Find matches in the given season that have no results
    upcoming_matches = Match.objects.filter(match_day__season__season_number=season_number, result__isnull=True)

    if not upcoming_matches.exists():
        raise ValueError("No upcoming matches found with no results for the given season.")

    # Identify the last completed match day
    last_completed_match_day = \
    Match.objects.filter(match_day__season__season_number=season_number, result__isnull=False).aggregate(
        Max('match_day__match_day_number'))['match_day__match_day_number__max']

    if last_completed_match_day is None:
        raise ValueError("No completed match days found for the given season.")

    # Gather the stats for each team as of the last completed match day
    form_data = MatchDayFormTable.objects.filter(match_day__season__season_number=season_number,
                                                 match_day__match_day_number=last_completed_match_day)

    # Gather goal difference and other stats from SeasonPositionTable
    position_data = SeasonPositionTable.objects.filter(season__season_number=season_number)

    # Convert form_data and position_data to dictionaries for quick lookup
    form_dict = {form.team.name: form for form in form_data}
    position_dict = {pos.team.name: pos for pos in position_data}

    # Prepare the DataFrame for upcoming matches
    data = []
    for match in upcoming_matches:
        home_team_name = match.home_team.name
        away_team_name = match.away_team.name

        home_form = form_dict.get(home_team_name, None)
        away_form = form_dict.get(away_team_name, None)
        home_position = position_dict.get(home_team_name, None)
        away_position = position_dict.get(away_team_name, None)

        if not home_form or not away_form or not home_position or not away_position:
            continue  # Skip if stats are missing for either team

        # Add match stats to the data list
        data.append({
            'Season': season_number,
            'Match Day ID': match.match_day.match_day_number,
            'Match Day': match.match_day.match_day,
            'Home Team': home_team_name,
            'Away Team': away_team_name,
            'Home Team Wins': home_form.wins,
            'Home Team Draws': home_form.draws,
            'Home Team Losses': home_form.losses,
            'Home Team Streak': home_form.streak,
            'Home Team Position': home_form.team_position,
            'Away Team Wins': away_form.wins,
            'Away Team Draws': away_form.draws,
            'Away Team Losses': away_form.losses,
            'Away Team Streak': away_form.streak,
            'Away Team Position': away_form.team_position,
            'Home Team Goal Difference': home_position.goal_difference,
            'Home Team Goals For': home_position.goals_for,
            'Home Team Goals Against': home_position.goals_against,
            'Home Team Points': home_position.points,
            'Away Team Goal Difference': away_position.goal_difference,
            'Away Team Goals For': away_position.goals_for,
            'Away Team Goals Against': away_position.goals_against,
            'Away Team Points': away_position.points
        })

    # Create a DataFrame from the data list
    df = pd.DataFrame(data)

    # Encode team names using the loaded label encoder
    df['Home Team'] = label_encoder.transform(df['Home Team'])
    df['Away Team'] = label_encoder.transform(df['Away Team'])

    return df


def train_model(X, y, label_encoder):
    X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)
    clf = RandomForestClassifier(n_estimators=100, random_state=42)
    clf.fit(X_train, y_train)

    y_pred = clf.predict(X_test)
    accuracy = accuracy_score(y_test, y_pred)
    report = classification_report(y_test, y_pred)

    # Save the model and label encoder
    model_filename = 'football_prediction_model.pkl'
    label_encoder_filename = 'label_encoder.pkl'
    joblib.dump(clf, model_filename)
    joblib.dump(label_encoder, label_encoder_filename)

    return clf, accuracy, report


def make_prediction(new_data, model_filename='football_prediction_model.pkl',
                    label_encoder_filename='label_encoder.pkl'):
    # Load the model
    clf = joblib.load(model_filename)
    label_encoder = joblib.load(label_encoder_filename)

    X_new = new_data[FEATURES]

    # Make prediction
    prediction = clf.predict(X_new)
    result = label_encoder.inverse_transform(prediction)

    return result


def train_and_save_model():
    X, y, label_encoder = preprocess_training_data()
    clf, accuracy, report = train_model(X, y, label_encoder)
    return clf, accuracy, report


def predict_results(season_number):
    new_data = create_prediction_dataframe(season_number)
    predictions = make_prediction(new_data)
    return predictions


def scrape_season_data(season_number, update=False):
    season_number = season_number
    season = Season.objects.get(season_number=season_number)
    ex_data = scrape_data(season)
    save_data_to_database(season.id, ex_data, update=update)


def main():
    season = 11781
    clf, accuracy, report = train_and_save_model()
    predictions = predict_results(season_number=season)

    return predictions, accuracy, report
