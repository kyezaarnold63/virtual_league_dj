from django.db import models


class Season(models.Model):
    name = models.CharField(max_length=100)
    season_number = models.PositiveIntegerField(unique=True)
    starting_match_day = models.PositiveIntegerField()
    end_match_day = models.PositiveIntegerField()

    def __str__(self):
        return self.name


class Team(models.Model):
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class MatchDay(models.Model):
    match_day_number = models.PositiveIntegerField(unique=True)
    match_day = models.PositiveIntegerField()
    season = models.ForeignKey(Season, on_delete=models.CASCADE)

    def __str__(self):
        return f'Match Day {self.match_day_number} of {self.season}'


class Match(models.Model):
    class Result(models.TextChoices):
        HOME_WIN = 'H', 'Home Win'
        AWAY_WIN = 'A', 'Away Win'
        DRAW = 'D', 'Draw'

    home_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='home_team')
    away_team = models.ForeignKey(Team, on_delete=models.CASCADE, related_name='away_team')
    home_team_half_time_goals = models.PositiveIntegerField('Home Team HT Goals', null=True, blank=True)
    away_team_half_time_goals = models.PositiveIntegerField('Away Team HT Goals', null=True, blank=True)
    home_team_full_time_goals = models.PositiveIntegerField('Home Team FT Goals', null=True, blank=True)
    away_team_full_time_goals = models.PositiveIntegerField('Away Team FT Goals', null=True, blank=True)
    match_day = models.ForeignKey(MatchDay, on_delete=models.CASCADE, related_name='matches')
    result = models.CharField(max_length=1, choices=Result.choices, null=True, blank=True)

    def __str__(self):
        return f'{self.home_team} vs {self.away_team}'

    class Meta:
        unique_together = ('home_team', 'away_team', 'match_day')
        verbose_name_plural = 'Matches'
        ordering = ('match_day__match_day', 'home_team__name')


class MatchDayFormTable(models.Model):
    match_day = models.ForeignKey(MatchDay, on_delete=models.CASCADE)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    wins = models.PositiveIntegerField(default=0)
    draws = models.PositiveIntegerField(default=0)
    losses = models.PositiveIntegerField(default=0)
    streak = models.IntegerField(default=0)
    team_position = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = ('match_day', 'team')
        verbose_name = 'Match Day Form'
        verbose_name_plural = 'Match Day Form'
        ordering = ('match_day__match_day', 'team__name')


class SeasonPositionTable(models.Model):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    position = models.PositiveIntegerField(default=0)
    team = models.ForeignKey(Team, on_delete=models.CASCADE)
    wins = models.PositiveIntegerField(default=0)
    draws = models.PositiveIntegerField(default=0)
    losses = models.PositiveIntegerField(default=0)
    goals_for = models.PositiveIntegerField(default=0)
    goals_against = models.PositiveIntegerField(default=0)
    goal_difference = models.IntegerField(default=0)
    points = models.PositiveIntegerField(default=0)

    class Meta:
        unique_together = ('season', 'team')
        verbose_name = 'League Table'
        verbose_name_plural = 'League Table'
        ordering = ('season__season_number', 'position')
